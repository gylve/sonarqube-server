# sonarqube-server

My sonarqube server setup using docker and docker-compose

Sonarqube version: 8.4.1

Before starting

execute as root

 > sudo sysctl -w vm.max_map_count=262144

For the moment download sonarqube .zip and copy plugins in this folder 

 > $WORKDIR/sonarqube_extensions/plugins


execute:
 > docker-compose up
